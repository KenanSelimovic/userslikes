import logger from '../lib/logger';

const prepare = require('mocha-prepare');

const DBHelper = require('../data/lib/db/DB').default;
const bookshelfBootstrap = require('../data/lib/db/bookshelfBootstrap');

prepare((done) => DBHelper.dropDatabase(process.env.DATABASE_DATABASE)
  .then(() => DBHelper.createDatabase(process.env.DATABASE_DATABASE))
  .then(bookshelfBootstrap.migrate)
  .then(done)
  .catch(logger.error));
