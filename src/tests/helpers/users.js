import iocContainer from '../../iocContainer';
import User from '../../data/users/entities/User';
import UserLike from '../../data/users/entities/UserLike';
import generateToken from '../../api/lib/passport/generateToken';
import { usersLikesRepository } from '../../data/users/repositories';

const {
  /** @type GenericRepository<User> */
  usersRepository,
} = iocContainer.cradle;

export const generateRandomUsername = () => `user-${Date.now()}-${Math.random()}-${Math.random()}`;

export const generateRandomUser = (data = {}) => new User({
  username: generateRandomUsername(),
  password: 'random-hash',
  ...data,
});

export const createUser = (data = {}) => usersRepository.insert(generateRandomUser(data));

/**
 * @param {User} user
 * @return {string}
 */
export const getLoginToken = (user) => generateToken(user.id);

/**
 * @param {Object} data
 * @return {Promise<{loginToken: string, user: User}>}
 */
export const createUserAndLoginToken = async (data = {}) => {
  const user = await createUser(data);
  const loginToken = getLoginToken(user);

  return { user, loginToken };
};

/**
 *
 * @param {number} userId
 */
export const likeUser = async (userId) => {
  const userLiking = await createUser();
  const userLike = UserLike.fromUsers(new User({ id: userId }), userLiking);
  await usersLikesRepository.insert(userLike);
};
