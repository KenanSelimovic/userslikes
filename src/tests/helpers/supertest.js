import request from 'supertest';
import { app as appBootstrap } from '../../api/server';

export default () => request(appBootstrap());
