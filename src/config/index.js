import path from 'path';

import Env from '../lib/env';

require('dotenv').config();

const rootPath = process.env.ROOTPATH || path.normalize(path.join(__dirname, '/../..'));

const config = {
  host: process.env.HOST || 'http://localhost',
  port: process.env.PORT || 8080,

  passport: {
    jwt_secret: Env.getEnv('JWT_SECRET', '', true),
  },

  // Database configuration
  db: {
    client: 'mysql',
    connection: {
      host: Env.getEnv('DATABASE_HOST', 'localhost'),
      database: Env.getEnv('DATABASE_DATABASE', '', true),
      user: Env.getEnv('DATABASE_USER'),
      password: Env.getEnv('DATABASE_PASSWORD'),
      port: Env.getEnvAsInt('DATABASE_PORT', 3306),
    },
    pool: {
      min: 2,
      max: 10,
    },
    seeds: {
      directory: `${rootPath}/src/data/seeds`,
    },
    migrations: {
      tableName: 'knex_migrations',
      directory: `${rootPath}/src/data/migrations`,
    },
  },
  databaseMigrationsAutorun: Env.getEnvAsBool('DATABASE_MIGRATIONS_AUTORUN', true),

  logLevel: Env.getEnv('LOG_LEVEL', 'debug'),

  isDevelopment: () => {
    const envVal = Env.getEnv('NODE_ENV');
    return envVal === '' || envVal === 'development';
  },

  isStaging: () => Env.getEnv('NODE_ENV') === 'staging',

  isProduction: () => Env.getEnv('NODE_ENV') === 'production',

  isTesting: () => Env.getEnvAsBool('IS_TESTING', false),
};

export default config;
