/**
 * @class UserLike
 */
export default class UserLike {
  /**
   * @type {number|null}
   */
  id;

  /**
   * @type {number}
   */
  likedUserId;

  /**
   * @type {number}
   */
  likingUserId;

  /**
   * @type {Date|undefined}
   */
  createdAt;

  /**
   * @type {Date|undefined}
   */
  updatedAt;

  constructor(values) {
    const possibleValues = [
      'id',
      'likedUserId',
      'likingUserId',
      'createdAt',
      'updatedAt',
    ];
    Object.keys(values).forEach((key) => {
      if (possibleValues.indexOf(key) !== -1 && typeof values[key] !== 'undefined') {
        this[key] = values[key];
      }
    });
  }

  /**
   * @param {User} userToLike
   * @param {User} userLiking
   * @returns {UserLike}
   */
  static fromUsers = (userToLike, userLiking) => {
    const userLike = new this({});

    userLike.likedUserId = userToLike.id;
    userLike.likingUserId = userLiking.id;

    return userLike;
  }
}
