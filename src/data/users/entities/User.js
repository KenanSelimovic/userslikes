/**
 * @class User
 */
export default class User {
  /**
   * @type {number|undefined}
   */
  id;

  /**
   * @type string
   */
  username;

  /**
   * @type string
   */
  password;

  /**
   * @type Array<UserLike>
   */
  likesGiven;

  /**
   * @type Array<UserLike>
   */
  likesReceived;

  /**
   * @type {Date|undefined}
   */
  createdAt;

  /**
   * @type {Date|undefined}
   */
  updatedAt;

  constructor(values) {
    const possibleValues = [
      'id',
      'username',
      'password',
      'likesGiven',
      'likesReceived',
      'createdAt',
      'updatedAt',
    ];
    Object.keys(values).forEach((key) => {
      if (possibleValues.indexOf(key) !== -1 && typeof values[key] !== 'undefined') {
        this[key] = values[key];
      }
    });
  }
}
