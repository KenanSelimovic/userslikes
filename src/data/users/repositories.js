import GenericRepository from '../lib/db/GenericRepository';
import User from './entities/User';
import UserLike from './entities/UserLike';
import UserModel from './models/UserModel';
import UserAuthDataModel from './models/UserAuthDataModel';
import UserLikeModel from './models/UserLikeModel';

/** @type GenericRepository<User> */
export const usersRepository = new GenericRepository(
  UserModel,
  User,
  ['likesGiven', 'likesReceived'],
);

/** @type GenericRepository<User> */
export const usersAuthDataRepository = new GenericRepository(
  UserAuthDataModel,
  User,
  ['likesGiven', 'likesReceived'],
);

/** @type GenericRepository<UserLike> */
export const usersLikesRepository = new GenericRepository(UserLikeModel, UserLike);
