/* eslint class-methods-use-this: 0 */
import { bookshelf } from '../../lib/db/bookshelfBootstrap';
import UserLikeModel from './UserLikeModel';

/** @type typeof Bookshelf.Model */
export default class UserModel extends bookshelf.Model {
  get tableName() { return 'users'; }

  get hidden() { return ['password']; }

  get hasTimestamps() { return ['createdAt', 'updatedAt']; }

  likesReceived() {
    return this.hasMany(UserLikeModel, 'likedUserId');
  }

  likesGiven() {
    return this.hasMany(UserLikeModel, 'likingUserId');
  }
}
