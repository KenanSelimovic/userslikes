/* eslint class-methods-use-this: 0 */
import UserModel from './UserModel';

/**
 * Represents user model to be used with auth.
 * Regular user model doesn't include password data and the
 * only time we don't want that is when handling authentication,
 * in which case this model should be used
 * @type typeof Bookshelf.Model
 */
export default class UserAuthDataModel extends UserModel {
  get hidden() { return []; }
}
