/* eslint class-methods-use-this: 0 */
import { bookshelf } from '../../lib/db/bookshelfBootstrap';

/** @type typeof Bookshelf.Model */
export default class UserLikeModel extends bookshelf.Model {
  get tableName() { return 'users_likes'; }

  get hasTimestamps() { return ['createdAt', 'updatedAt']; }
}
