/**
 * @class UsersDA
 */
export default class UsersDA {
  /**
   * @type GenericRepository<User>
   * @private
   */
  _repository;

  /**
   * @type GenericRepository<User>
   * @private
   */
  _usersAuthDataRepository;

  /**
   * @type GenericRepository<UserLike>
   * @private
   */
  _userLikesRepository;

  constructor(usersRepository, usersAuthDataRepository, usersLikesRepository) {
    this._repository = usersRepository;
    this._usersAuthDataRepository = usersAuthDataRepository;
    this._userLikesRepository = usersLikesRepository;
  }

  /**
   * @param {DataAccessOptions} [options]
   * @returns {Promise<Array<User>>}
   */
  getAll = (options = {}) => this._repository.findAll(options);

  /**
   * @param {number} id
   * @param {DataAccessOptions} [options]
   * @return {Promise<User|null>}
   */
  getById = (id, options = {}) => this._repository.findById(id, options);

  /**
   * @param {string} username
   * @return {Promise<User|null>}
   */
  getByUsername = (username) => this._repository.findOne({ username });

  /**
   * @param {string} username
   * @return {Promise<User|null>}
   */
  getWithAuthDataByUsername = (username) => this._usersAuthDataRepository.findOne({ username });

  /**
   * @param {User} user
   * @return {Promise<User>}
   */
  insertUser = (user) => this._repository.insert(user);

  /**
   * @param {User} user
   * @return {Promise<User>}
   */
  updateUser = (user) => this._usersAuthDataRepository.update(user);

  /**
   *
   * @param userLike {UserLike}
   * @returns {UserLike}
   */
  insertUserLike = async (userLike) => this._userLikesRepository.insert(userLike);

  /**
   *
   * @param userLike {UserLike}
   * @returns {UserLike}
   */
  deleteUserLike = async (userLike) => this._userLikesRepository.delete(userLike);

  /**
   *
   * @param {number} likedUserId
   * @param {number} likingUserId
   * @returns {UserLike}
   */
  getUserLike = async (likedUserId, likingUserId) => this._userLikesRepository.findOne({
    likedUserId, likingUserId,
  });
}
