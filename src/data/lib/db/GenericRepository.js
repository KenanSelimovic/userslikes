import assert from 'assert';

import { bookshelf } from './bookshelfBootstrap';

/** @typedef {knex.Transaction} DBTransaction */
/**
 * @typedef DataAccessOptions
 * @type {object}
 * @property {DBTransaction | undefined} transaction
 * @property {Array<string> | undefined} withRelated
 */

/**
 * @template T
 */
export default class GenericRepository {
  /**
   * Bookshelf model (not an instance)
   *
   * @type typeof Bookshelf.Model
   * @protected
   */
  _Model;

  /**
   * Our entity class (not an instance)
   *
   * @type {T}
   * @protected
   */
  _ModelClass;

  /**
   * Which properties to remove when creating bookshelf model
   * out of our entity class
   *
   * @type Array<string>
   * @protected
   */
  attributesToRemoveOnModelCreation = ['createdAt', 'updatedAt'];


  /**
   * if keys are entities themselves, we want them to be of proper class
   * For example, if we want to be able to do event.booking.getTotalPrice()
   * we need to pass to EventsRepository [{ key: 'booking', entityClass: Booking }]
   * with 'Booking' being our entity class (not an instance)
   *
   * @type {Array<{key: string, entityClass: Class}>}
   * @protected
   */
  _knownEntityKeys;

  /**
   * When not to do eg. model.eventId = data.event.id, which
   * is the default behaviour
   *
   * @type Array<string>
   * @protected
   */
  _keysToNotSaveUnderIdKey;

  /**
   * @type string
   * @protected
   */
  _idKey;

  /**
   * When read from db, all boolean values will be 1 or 0.
   * Since we want them to be regular JS booleans, we pass those
   * properties' names here so they can be transformed on entity creation.
   *
   * @type Array<string>
   * @protected
   */
  _booleanValues;

  /**
   *
   * @param {Class} Model
   * @param {Class} [ModelClass]
   * @param {Array<string>} [propsToRemoveOnModelCreation]
   * @param {Array<{key: string, entityClass: Class}>} [knownEntityKeys]
   * Transform these properties into class instances, eg. booking.event
   * @param {Array<string>} [keysToNotSaveUnderIdKey] Not doing like
   * booking.eventId = booking.event.id
   * @param {string} [idKey] Primary key
   * @param {Array<string>} [booleanValues] For these properties,we will transform 1/0 to true/false
   */
  constructor(
    Model,
    ModelClass,
    propsToRemoveOnModelCreation = [],
    knownEntityKeys = [],
    keysToNotSaveUnderIdKey = [],
    idKey = 'id',
    booleanValues = [],
  ) {
    this._Model = Model;
    this._ModelClass = ModelClass;
    this.attributesToRemoveOnModelCreation = this.attributesToRemoveOnModelCreation
      .concat(propsToRemoveOnModelCreation);
    this._knownEntityKeys = knownEntityKeys;
    this._keysToNotSaveUnderIdKey = keysToNotSaveUnderIdKey;
    this._idKey = idKey;
    this._booleanValues = booleanValues;
  }

  /**
   * @param {Object} query
   * @param {DataAccessOptions} options
   * @returns {Promise<T|null>}
   */
  async findOne(query, options = {}) {
    const model = await new this._Model(query)
      .query((q) => {
        if (options.transaction) {
          q.transacting(options.transaction).forUpdate();
        }
      })
      .fetch({
        withRelated: options.withRelated || [],
        require: false,
      });
    if (!model) {
      return null;
    }
    return this.modelToClass(model);
  }

  /**
   * @param {number|string} id
   * @param {DataAccessOptions} options
   * @returns {Promise<T|null>}
   */
  findById(id, options = {}) {
    return this.findOne({ id }, options);
  }

  /**
   * @param {Object} query
   * @param {DataAccessOptions} options
   * @returns {Promise<Array<T>>}
   */
  async find(query, options = {}) {
    const result = await this._Model.query({
      where: query,
    })
      .fetchAll({
        withRelated: options.withRelated || [],
        transacting: options.transaction,
      });
    return result.models.map((m) => this.modelToClass(m));
  }

  /**
   * @param {DataAccessOptions} options
   * @returns {Promise<Array<T>>}
   */
  async findAll(options = {}) {
    const models = await this._Model.fetchAll({
      withRelated: options.withRelated || [],
      transacting: options.transaction,
    });
    return models.map((m) => this.modelToClass(m));
  }

  delete = (modelClass, options = {}) => this.classToModel(modelClass)
    .destroy({ transacting: options.transaction });

  /**
   * @param {T} modelClass
   * @param {DataAccessOptions} options
   * @returns {Promise<T>}
   */
  insert = async (modelClass, options = {}) => {
    const inserted = await this.classToModel(modelClass).save(null, { transacting: options.transaction, method: 'insert' });
    return this.modelToClass(inserted);
  };

  /**
   * @param {Array<T>} modelClasses
   * @param {DataAccessOptions} options
   * @returns {Promise<void>}
   */
  insertMany = async (modelClasses, options = {}) => {
    const toInsert = modelClasses.map((c) => this.classToModel(c).toJSON());
    let query = bookshelf.knex(this.getTableName());
    if (options.transaction) {
      query = query.transacting(options.transaction);
    }
    await query.insert(toInsert);
  };

  /**
   * @param {T} modelClass
   * @param {DataAccessOptions} options
   * @returns {Promise<T>}
   */
  update = async (modelClass, options = {}) => {
    const updated = await this.classToModel(modelClass).save(null, { transacting: options.transaction, method: 'update' });
    return this.modelToClass(updated);
  };

  /**
   * @param {T} modelClass
   * @param {DataAccessOptions} options
   * @returns {Promise<T>}
   */
  insertOrUpdate = async (modelClass, options = {}) => {
    const existing = await this.findOne({
      [this._idKey]: modelClass[this._idKey],
    });
    const changed = await this.classToModel(modelClass).save(
      null,
      { transacting: options.transaction, method: existing ? 'update' : 'insert' },
    );
    return this.modelToClass(changed);
  };


  classToModel = (data) => {
    const model = new this._Model(data);
    this.attributesToRemoveOnModelCreation.forEach((prop) => {
      model.unset(prop);
      if (data[prop] && data[prop].id && this._keysToNotSaveUnderIdKey.indexOf(prop) === -1) {
        model.set(`${prop}Id`, data[prop].id);
      }
    });
    return model;
  };

  modelToClass = (model) => {
    const classValue = this._ModelClass
      ? new this._ModelClass(model.toJSON())
      : model.toJSON();

    this._knownEntityKeys.forEach(({ key, entityClass: EntityClass }) => {
      assert.ok(key);
      assert.ok(EntityClass);
      if (classValue[key]) {
        if (Array.isArray(classValue[key])) {
          classValue[key] = classValue[key].map((item) => new EntityClass(item));
        } else {
          classValue[key] = new EntityClass(classValue[key]);
        }
      }
    });
    this._booleanValues.forEach((keyName) => {
      const value = model.get(keyName);
      if (typeof value === 'number') {
        classValue[keyName] = value === 1;
      }
    });
    return classValue;
  };

  /**
   * Returns table name of model using which this repository has been instantiated
   *
   * @returns {string}
   * @protected
   */
  getTableName = () => new this._Model().tableName;
}
