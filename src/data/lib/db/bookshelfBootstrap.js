import bs from 'bookshelf';
import knex from 'knex';

import config from '../../../config';
import logger from '../../../lib/logger';

const { db: dbConfig } = config;
const knexConnection = knex(config.db);

const bookshelfInstance = bs(knexConnection);

logger.info(`Bookshelf connected to DB at ${dbConfig.connection.host}:${dbConfig.connection.port}`);

export const migrate = async () => {
  await knexConnection.migrate.latest(config.db.migrations);
  logger.info('Migrated to latest DB version successfully');
};

/**
 * @type {Bookshelf}
 */
export const bookshelf = bookshelfInstance;
