import mysql from 'mysql';

import { bookshelf } from './bookshelfBootstrap';
import config from '../../../config';

/** @class DB */
export default class DB {
  static _defaultDbConfig = {
    host: config.db.connection.host,
    port: config.db.connection.port,
    user: config.db.connection.user,
    password: config.db.connection.password,
  };

  static transact = bookshelf.knex.transaction.bind(bookshelf.knex);

  static createDatabase(dbName) {
    const escapedDbName = dbName.replace(/"/g, '""');
    return DB._executeRawQuery(`CREATE DATABASE IF NOT EXISTS ${escapedDbName};`);
  }

  static dropDatabase = (dbName) => DB._executeRawQuery(`DROP DATABASE IF EXISTS ${dbName};`);

  static _executeRawQuery = (query, customDbConfig = {}) => new Promise((resolve, reject) => {
    DB._getMysqlClient(customDbConfig).then((client) => {
      client.query(query, (err, res) => {
        if (err) {
          reject(err);
        }
        resolve(res);
      });
    });
  });


  static _getMysqlClient(dbConfig = {}) {
    const fullConfig = {
      ...DB._defaultDbConfig,
      ...dbConfig,
    };
    return new Promise((resolve, reject) => {
      let client = mysql.createConnection(fullConfig);

      client.connect((err) => {
        if (err) {
          reject(err);
        }
      });

      client.on('error', (err) => {
        if (err.code === 'PROTOCOL_CONNECTION_LOST') {
          client = mysql.createConnection(fullConfig);
        } else {
          reject(err);
        }
      });
      resolve(client);
    });
  }
}
