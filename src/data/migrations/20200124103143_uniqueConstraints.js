exports.up = async function up(knex) {
  await knex.schema.table('users', (t) => {
    t.unique('username');
  });
  await knex.schema.table('users_likes', (t) => {
    t.unique(['likedUserId', 'likingUserId']);
  });
};

exports.down = async function down(knex) {
  await knex.schema.table('users', (t) => {
    t.dropUnique('username');
  });
  await knex.schema.table('users_likes', (t) => {
    t.dropUnique(['likedUserId', 'likingUserId']);
  });
};
