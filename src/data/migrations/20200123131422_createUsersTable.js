exports.up = function up(knex) {
  return knex.schema
    .createTable('users', (t) => {
      t.increments().primary();
      t.string('username').notNullable();
      t.string('password').notNullable();
      t.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
      t.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = function down(knex) {
  return knex.schema
    .dropTable('users');
};
