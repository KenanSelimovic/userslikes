exports.up = function up(knex) {
  return knex.schema
    .createTable('users_likes', (t) => {
      t.increments().primary();
      t.integer('likedUserId').unsigned().references('id').inTable('users')
        .notNullable();
      t.integer('likingUserId').unsigned().references('id').inTable('users')
        .notNullable();
      t.dateTime('createdAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'));
      t.dateTime('updatedAt').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP'));
    });
};

exports.down = function down(knex) {
  return knex.schema
    .dropTable('users_likes');
};
