// inspired by "specification pattern"
// used to name specific data requirement to make it possible
// to keep track of different needs for different use cases
// eslint-disable-next-line import/prefer-default-export
export class StandardUserDataSpecification {
  withRelated = [
    'likesReceived',
    'likesGiven',
  ]
}
