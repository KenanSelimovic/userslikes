import { StandardUserDataSpecification } from './specifications';
import UserLike from '../../data/users/entities/UserLike';
import User from '../../data/users/entities/User';

/** @class UsersService */
export default class UsersService {
  /**
   * @private
   * @type {UsersDA}
   */
  _usersDA;

  constructor(usersDA) {
    this._usersDA = usersDA;
  }

  /**
   * @param {number} id
   * @return {Promise<User|null>}
   */
  getUserById = (id) => this._usersDA.getById(id, new StandardUserDataSpecification());

  /**
   * Returns all users sorted in descending by number of likes they've received
   * @return {Promise<Array<User>>}
   */
  getMostLiked = async () => {
    const allUsers = await this._usersDA.getAll(new StandardUserDataSpecification());
    return allUsers.sort((u1, u2) => (u1.likesReceived > u2.likesReceived ? -1 : 1));
  };

  /**
   * @param {User} userLiking
   * @param {number} userToLikeId
   * @returns {Promise<UserLike>}
   */
  likeUser = async (userToLikeId, userLiking) => {
    if (userToLikeId === userLiking.id) {
      throw new Error('USER_LIKING_HIMSELF');
    }

    const userToLike = await this._usersDA.getById(userToLikeId);
    if (userToLike === null) {
      throw new Error('NOT_FOUND');
    }

    const userWithLikes = await this._usersDA.getById(
      userLiking.id,
      new StandardUserDataSpecification(),
    );
    const likeForSameUser = userWithLikes.likesGiven.find((l) => l.likedUserId === userToLikeId);
    if (likeForSameUser !== undefined) {
      throw new Error('USER_ALREADY_LIKED');
    }

    const userLike = UserLike.fromUsers(new User({ id: userToLikeId }), userLiking);
    return this._usersDA.insertUserLike(userLike);
  };

  /**
   * @param {number} likedUserId
   * @param {number} userUnlikingId
   * @returns {Promise<UserLike>}
   */
  unlikeUser = async (likedUserId, userUnlikingId) => {
    const userLike = await this._usersDA.getUserLike(likedUserId, userUnlikingId);
    if (userLike === null) {
      throw new Error('LIKE_NOT_FOUND');
    }
    return this._usersDA.deleteUserLike(userLike);
  }
}
