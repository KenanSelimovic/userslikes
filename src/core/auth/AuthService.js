import bcrypt from 'bcrypt';

import User from '../../data/users/entities/User';

/** @class AuthService */
export default class AuthService {
  /**
   * @private
   * @type {UsersDA}
   */
  _usersDA;

  constructor(usersDA) {
    this._usersDA = usersDA;
  }

  /**
   * @param {User} user
   * @return {Promise<User>}>}
   */
  registerUser = async (user) => {
    const userCopy = new User(user);
    const existingUser = await this._usersDA.getWithAuthDataByUsername(userCopy.username);

    if (existingUser) {
      throw new Error('DUPLICATE_USERNAME');
    }

    userCopy.password = await this._hashPassword(userCopy.password);

    return this._usersDA.insertUser(userCopy);
  };

  /**
   * @param {string} password
   * @return {Promise<string>}
   * @private
   */
  _hashPassword = async (password) => {
    const salt = await bcrypt.genSalt(10);
    return bcrypt.hash(password, salt);
  };

  /**
   * @param {string} username
   * @param {string} password
   * @return {Promise<User>}>}
   */
  getUser = async (username, password) => {
    const user = await this._usersDA.getWithAuthDataByUsername(username);

    if (!user) {
      throw new Error('NOT_FOUND');
    }

    const comparisonResult = await bcrypt.compare(password, user.password);

    if (comparisonResult !== true) {
      throw new Error('NOT_FOUND');
    }
    delete user.password;

    return user;
  };

  /**
   * @param {User} user
   * @param {string} password
   * @return {Promise<User>}>}
   */
  changePassword = async (user, password) => {
    const userCopy = new User(user);

    userCopy.password = await this._hashPassword(password);

    return this._usersDA.updateUser(userCopy);
  };
}
