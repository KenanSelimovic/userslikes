import * as awilix from 'awilix/lib/awilix';
import UsersController from './api/users/UsersController';
import UsersService from './core/users/UsersService';
import UsersDA from './data/users/UsersDA';
import {
  usersAuthDataRepository,
  usersLikesRepository,
  usersRepository,
} from './data/users/repositories';
import AuthController from './api/auth/AuthController';
import AuthService from './core/auth/AuthService';
import MeController from './api/me/MeController';

/*
 * All app dependencies are listed here
 */
const iocContainer = awilix.createContainer({
  injectionMode: awilix.InjectionMode.CLASSIC,
});

iocContainer.register({
  usersController: awilix.asClass(UsersController),
  usersService: awilix.asClass(UsersService),
  usersDA: awilix.asClass(UsersDA),
  usersRepository: awilix.asValue(usersRepository),
  authController: awilix.asClass(AuthController),
  authService: awilix.asClass(AuthService),
  usersAuthDataRepository: awilix.asValue(usersAuthDataRepository),
  meController: awilix.asClass(MeController),
  usersLikesRepository: awilix.asValue(usersLikesRepository),
});

export default iocContainer;
