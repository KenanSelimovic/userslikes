import bodyParser from 'body-parser';
import express from 'express';

import config from '../config';
import logger from '../lib/logger';
import globalErrorHandler from './lib/error/globalErrorHandler';
import routes from './routes';

export function app(passedInstance = null) {
  const instance = passedInstance || express();

  instance
    .use(bodyParser.urlencoded({ parameterLimit: 10000, limit: '10mb', extended: true }))
    .use(bodyParser.json({ parameterLimit: 10000, limit: '10mb' }))
    .use(globalErrorHandler());

  instance.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.setHeader('Access-Control-Allow-Credentials', true);
    res.setHeader('Access-Control-Allow-Headers', 'Content-Type, set-cookie');
    next();
  });

  instance
    .use('/', routes);

  return instance;
}

export function serve() {
  app().listen(config.port);

  logger.info(`Server started listening on port ${config.port}`);
  logger.info('Runtime environment:', process.env.NODE_ENV);
}
