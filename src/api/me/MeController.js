import RequestResponder from '../lib/validation/RequestResponder';

/** @class MeController */
export default class MeController {
  getCurrentUser = async (req, res) => {
    const requestResponder = new RequestResponder(res);
    requestResponder.success(req.user);
  };

  updatePassword = async (req, res, next) => {
    const { password } = req.body;
    const {
      /** @type AuthService */
      authService,
    } = req.context;

    try {
      await authService.changePassword(req.user, password);
      new RequestResponder(res).success();
    } catch (err) {
      next(err);
    }
  };
}
