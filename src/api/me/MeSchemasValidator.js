import {
  celebrate,
  errors,
  Joi,
  Segments,
} from 'celebrate';
import { compose } from 'compose-middleware';

const passwordChangeSchema = Joi.object().keys({
  password: Joi.string().min(3).required(),
});

/**
 *  Validate incoming request
 *  to make sure controllers are always working with valid data
 */
export default class MeSchemasValidator {
  static updatePassword = compose([
    celebrate({
      [Segments.BODY]: passwordChangeSchema,
    }),
    errors(),
  ]);
}
