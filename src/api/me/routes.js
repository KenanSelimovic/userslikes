import { Router } from 'express';

import requireAuth from '../lib/passport/requireAuth';
import MeSchemasValidator from './MeSchemasValidator';

export default (IOCContainer) => {
  const controller = IOCContainer.cradle.meController;
  return Router({ strict: true })
    .get('/me', requireAuth, controller.getCurrentUser)
    .post('/me/update-password', requireAuth, MeSchemasValidator.updatePassword, controller.updatePassword);
};
