import { assert } from 'chai';

import {
  createUserAndLoginToken,
} from '../../../tests/helpers/users';
import getSupertest from '../../../tests/helpers/supertest';

const supertest = getSupertest();

describe('Me controller', () => {
  describe('Get current user', () => {
    it('getCurrentUser_everythingOK_returnsUsername', async () => {
      // arrange
      const { user, loginToken } = await createUserAndLoginToken();

      // act
      const response = await supertest
        .get('/me')
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 200);
      assert.equal(response.body.data.username, user.username);
    });
    it('getCurrentUser_notLoggedIn_returns401', async () => {
      // act
      const response = await supertest
        .get('/me');

      // assert
      assert.equal(response.statusCode, 401);
    });
  });
});
