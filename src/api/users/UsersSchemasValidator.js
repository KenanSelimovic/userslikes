import { celebrate, errors, Segments } from 'celebrate';
import { compose } from 'compose-middleware';

import { simpleIdInParamsSchema } from '../lib/validation/commonValidationSchemas';

/**
 *  Validate incoming request
 *  to make sure controllers are always working with valid data
 */
export default class UsersSchemasValidator {
  static getUser = compose([
    celebrate({
      [Segments.PARAMS]: simpleIdInParamsSchema,
    }),
    errors(),
  ]);

  static likeUser = compose([
    celebrate({
      [Segments.PARAMS]: simpleIdInParamsSchema,
    }),
    errors(),
  ]);

  static unlikeUser = compose([
    celebrate({
      [Segments.PARAMS]: simpleIdInParamsSchema,
    }),
    errors(),
  ]);
}
