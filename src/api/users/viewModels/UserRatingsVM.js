/** @class UserRatingsVM */
export default class UserRatingsVM {
  username;

  numLikes;

  constructor(username, numLikes) {
    this.username = username;
    this.numLikes = numLikes;
  }

  /**
   * @param {User} user
   * @returns {UserRatingsVM}
   */
  static fromUser = (user) => new UserRatingsVM(user.username, user.likesReceived.length)
}
