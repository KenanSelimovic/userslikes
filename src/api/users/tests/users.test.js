import { assert } from 'chai';

import {
  createUser,
  createUserAndLoginToken,
  getLoginToken,
  likeUser,
} from '../../../tests/helpers/users';
import getSupertest from '../../../tests/helpers/supertest';
import iocContainer from '../../../iocContainer';

const supertest = getSupertest();
const {
  /** @type GenericRepository<UserLike> */
  usersLikesRepository,
} = iocContainer.cradle;

describe('Users controller', () => {
  describe('Get user', () => {
    it('getUser_everythingOK_returnsUsernameAndNumLikes', async () => {
      // arrange
      const userToLike = await createUser();
      const numTimesToLike = 3;
      const likePromises = [];
      for (let i = 0; i < numTimesToLike; i += 1) {
        likePromises.push(likeUser(userToLike.id));
      }
      await Promise.all(likePromises);

      // act
      const response = await supertest
        .get(`/user/${userToLike.id}`);

      // assert
      assert.equal(response.statusCode, 200);
      const { username, numLikes } = response.body.data;
      assert.equal(username, userToLike.username);
      assert.equal(numLikes, numTimesToLike);
    });
    it('getUser_nonExistentUser_returns404', async () => {
      // arrange
      const user = await createUser(); // we get current max user id
      // act
      const response = await supertest
        .get(`/user/${user.id + 9999}`);

      // assert
      assert.equal(response.statusCode, 404);
    });
  });
  describe('Get most liked users', () => {
    it('getMostLikedUsers_everythingOK_returnsUsernameAndNumLikes', async () => {
      // arrange
      const userToLike = await createUser();
      await likeUser(userToLike.id);

      // act
      const response = await supertest
        .get('/most-liked');

      // assert
      assert.equal(response.statusCode, 200);
      const sampleUser = response.body.data[0];
      assert.isString(sampleUser.username);
      assert.isNumber(sampleUser.numLikes);
    });
    it('getMostLikedUsers_everythingOK_usersAreProperlySorted', async () => {
      // arrange
      const userToLike = await createUser();
      const likePromises = [];
      for (let i = 0; i < 2; i += 1) {
        likePromises.push(likeUser(userToLike.id));
      }
      await Promise.all(likePromises);

      // act
      const response = await supertest
        .get('/most-liked');

      // assert
      assert.equal(response.statusCode, 200);
      const users = response.body.data;
      const firstInOrder = users[0];
      const lastInOrder = users[users.length - 1];
      assert.isTrue(firstInOrder.numLikes >= lastInOrder.numLikes);
    });
  });
  describe('Like user', () => {
    it('likingUser_everythingOK_likeStored', async () => {
      // arrange
      const user = await createUser();
      const userToLike = await createUser();
      const loginToken = getLoginToken(user);

      // act
      const response = await supertest
        .post(`/user/${userToLike.id}/like`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 200);
      const insertedUserLike = await usersLikesRepository.findOne({
        likedUserId: userToLike.id,
        likingUserId: user.id,
      });
      assert.exists(insertedUserLike);
    });
    it('likingUser_userLikingHimself_400Returned', async () => {
      // arrange
      const user = await createUser();
      const loginToken = getLoginToken(user);

      // act
      const response = await supertest
        .post(`/user/${user.id}/like`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 400);
      const insertedUserLike = await usersLikesRepository.findOne({
        likedUserId: user.id,
        likingUserId: user.id,
      });
      assert.notExists(insertedUserLike);
    });
    it('likingUser_duplicateLike_400Returned', async () => {
      // arrange
      const user = await createUser();
      const userToLike = await createUser();
      const loginToken = getLoginToken(user);
      await supertest
        .post(`/user/${userToLike.id}/like`)
        .set('Authorization', loginToken);

      // act
      const response = await supertest
        .post(`/user/${userToLike.id}/like`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 400);
    });
    it('likingUser_userDoesntExist_404Returned', async () => {
      // arrange
      const { user, loginToken } = await createUserAndLoginToken();

      // act
      const response = await supertest
        .post(`/user/${user.id + 999}/like`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 404);
    });
  });
  describe('Unlike user', () => {
    it('unlikingUser_everythingOK_likeRemoved', async () => {
      // arrange
      const { user, loginToken } = await createUserAndLoginToken();
      const userToLike = await createUser();
      await supertest
        .post(`/user/${userToLike.id}/like`)
        .set('Authorization', loginToken);

      // act
      const response = await supertest
        .post(`/user/${userToLike.id}/unlike`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 200);
      const insertedUserLike = await usersLikesRepository.findOne({
        likedUserId: userToLike.id,
        likingUserId: user.id,
      });
      assert.notExists(insertedUserLike);
    });
    it('unlikingUser_likeDoesntExist_400Returned', async () => {
      // arrange
      const { loginToken } = await createUserAndLoginToken();
      const userToLike = await createUser();

      // act
      const response = await supertest
        .post(`/user/${userToLike.id}/unlike`)
        .set('Authorization', loginToken);

      // assert
      assert.equal(response.statusCode, 400);
    });
  });
});
