import { Router } from 'express';

import Schemas from './UsersSchemasValidator';
import requireAuth from '../lib/passport/requireAuth';

export default (IOCContainer) => {
  /** @type UsersController */
  const controller = IOCContainer.cradle.usersController;
  return Router({ strict: true })
    .get('/user/:id', Schemas.getUser, controller.getUser)
    .get('/most-liked', controller.getMostLiked)
    .post('/user/:id/like', requireAuth, Schemas.likeUser, controller.likeUser)
    .post('/user/:id/unlike', requireAuth, Schemas.unlikeUser, controller.unlikeUser);
};
