import RequestResponder from '../lib/validation/RequestResponder';
import UserRatingsVM from './viewModels/UserRatingsVM';

/** @class UsersController */
export default class UsersController {
  getUser = async (req, res, next) => {
    const {
      /** @type {UsersService} */
      usersService,
    } = req.context;

    const {
      id: userId,
    } = req.params;

    const requestResponder = new RequestResponder(res);

    try {
      const user = await usersService.getUserById(userId);

      if (user === null) {
        requestResponder.failure('NOT_FOUND');
      } else {
        requestResponder.success(UserRatingsVM.fromUser(user));
      }
    } catch (err) {
      next(err);
    }
  };

  getMostLiked = async (req, res, next) => {
    const {
      /** @type {UsersService} */
      usersService,
    } = req.context;

    const requestResponder = new RequestResponder(res);

    try {
      const users = await usersService.getMostLiked();
      const usersWithLikes = users.map(UserRatingsVM.fromUser);

      requestResponder.success(usersWithLikes);
    } catch (err) {
      next(err);
    }
  };

  likeUser = async (req, res, next) => {
    const {
      /** @type {UsersService} */
      usersService,
    } = req.context;
    const {
      id: userId,
    } = req.params;
    const requestResponder = new RequestResponder(res);

    try {
      const userLike = await usersService.likeUser(userId, req.user);
      requestResponder.success(userLike);
    } catch (err) {
      const expectedErrors = ['USER_LIKING_HIMSELF', 'USER_ALREADY_LIKED', 'NOT_FOUND'];
      if (expectedErrors.includes(err.message)) {
        requestResponder.failure(err.message);
      } else {
        next(err);
      }
    }
  };

  unlikeUser = async (req, res, next) => {
    const {
      /** @type {UsersService} */
      usersService,
    } = req.context;
    const {
      id: userId,
    } = req.params;
    const requestResponder = new RequestResponder(res);

    try {
      await usersService.unlikeUser(userId, req.user.id);
      requestResponder.success();
    } catch (err) {
      if (err.message === 'LIKE_NOT_FOUND') {
        requestResponder.failure('LIKE_NOT_FOUND');
      } else {
        next(err);
      }
    }
  };
}
