import passport from 'passport';

import './jwt';
import RequestResponder from '../validation/RequestResponder';

export default (req, res, next) => {
  passport.authenticate('jwt', { session: false }, (err, user) => {
    if (!user) {
      new RequestResponder(res).failure('NOT_AUTHORIZED');
      return;
    }
    req.user = user;
    next();
  })(req, res, next);
};
