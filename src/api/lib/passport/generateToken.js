import jwt from 'jwt-simple';

import config from '../../../config';

export default (userId) => {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: userId, iat: timestamp }, config.passport.jwt_secret);
};
