import passport from 'passport';
import { Strategy as JwtStrategy, ExtractJwt } from 'passport-jwt';

import config from '../../../config';
import logger from '../../../lib/logger';
import iocContainer from '../../../iocContainer';

const { usersDA } = iocContainer.cradle;

// setup options for jwt strategy
const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromHeader('authorization'),
  secretOrKey: config.passport.jwt_secret,
};

// create jwt strategy
const jwtLogin = new JwtStrategy(
  jwtOptions,
  async (payload, done) => {
    try {
      const user = await usersDA.getById(payload.sub);

      if (user) {
        done(null, user);
      } else {
        done(null, null);
      }
    } catch (err) {
      logger.log('error', err);
      done(err, null);
    }
  },
);

// tell passport to use this strategy
passport.use(jwtLogin);
