import assert from 'assert';

import APIErrors from '../error/APIErrors';
import APIResponse from './APIResponse';

export default class RequestResponder {
  /**
   * @private
   * @type {express.Response}
   */
  _response;

  /**
   * @param {express.Response} response
   */
  constructor(response) {
    this._response = response;
  }

  /**
   *
   * @param {string} errorCode
   * @param {*} [data]
   */
  failure = (errorCode, data) => {
    const error = APIErrors[errorCode];
    assert.ok(error, 'Error with provided error code exists');

    this._response
      .status(error.status)
      .send(APIResponse.error(error.code, error.message, data));
  };

  /**
   * @param {*} [data]
   * @param {string} [message]
   * @param {number} [statusCode]
   */
  success = (data, message = 'Success', statusCode = 200) => {
    this._response
      .status(statusCode)
      .send(APIResponse.success(data, message, statusCode));
  };
}
