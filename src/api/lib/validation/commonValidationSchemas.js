import { Joi } from 'celebrate';

// eslint-disable-next-line import/prefer-default-export
export const simpleIdInParamsSchema = Joi.object().keys({
  id: Joi.number().integer().min(1).required(),
});
