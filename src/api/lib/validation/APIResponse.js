export default class APIResponse {
  error;

  message;

  data;

  constructor(code, message = '', data) {
    this.error = code;
    this.message = message;
    this.data = data;
  }

  static error(code, message = '', data) {
    return new this(code, message, data);
  }

  static success(data, message = 'Success') {
    return new this(null, message, data);
  }
}
