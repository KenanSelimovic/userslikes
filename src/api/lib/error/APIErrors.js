export default {
  UNKNOWN_ERROR: {
    message: 'Unknown server error has occurred',
    code: 'UNKNOWN_ERROR',
    status: 500,
  },
  NOT_FOUND: {
    message: 'Requested resource doesn\'t exist',
    code: 'NOT_FOUND',
    status: 404,
  },
  NOT_AUTHORIZED: {
    message: 'Not authorized',
    code: 'NOT_AUTHORIZED',
    status: 401,
  },
  DUPLICATE_USERNAME: {
    message: 'Username already taken',
    code: 'DUPLICATE_USERNAME',
    status: 400,
  },
  USER_ALREADY_LIKED: {
    message: 'This user has already liked target user',
    code: 'USER_ALREADY_LIKED',
    status: 400,
  },
  USER_LIKING_HIMSELF: {
    message: 'User cant like himself/herself',
    code: 'USER_LIKING_HIMSELF',
    status: 400,
  },
  LIKE_NOT_FOUND: {
    message: 'Like not found',
    code: 'LIKE_NOT_FOUND',
    status: 400,
  },
};
