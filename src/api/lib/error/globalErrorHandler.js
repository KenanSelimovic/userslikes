/* eslint no-unused-vars:0 */
import logger from '../../../lib/logger';

function isXhrOrAcceptsJson(req) {
  return req.xhr || (req.headers && req.headers.acccept && req.headers.accept.indexOf('json') > -1);
}

function prepareErrorResponse(acceptsJson, error) {
  return acceptsJson ? {
    error,
  } : error;
}

/** Global error handler middleware to handle all general exceptions from controllers etc.
 It supports returning JSON responses for XHR requests and regular responses for normal requests */
const globalErrorHandler = () => (err, req, res, next) => {
  if (err) {
    const isXhr = isXhrOrAcceptsJson(req);

    if (err.name === 'UnauthorizedError') {
      res.status(401).send(prepareErrorResponse(isXhr, 'Acess Denied'));
    } else if (err.status === 404) {
      res.status(404).send(prepareErrorResponse(isXhr, 'Not Found'));
    } else {
      logger.error(err);
      res.status(500).send(prepareErrorResponse(isXhr, 'A Server error occurred'));
    }
  }
};

export default globalErrorHandler;
