import {
  celebrate,
  errors,
  Joi,
  Segments,
} from 'celebrate';
import { compose } from 'compose-middleware';

const authDataSchema = Joi.object().keys({
  username: Joi.string().min(3).required(),
  password: Joi.string().min(3).required(),
});

/**
 *  Validate incoming request
 *  to make sure controllers are always working with valid data
 */
export default class UsersSchemasValidator {
  static register = compose([
    celebrate({
      [Segments.BODY]: authDataSchema,
    }),
    errors(),
  ]);

  static login = compose([
    celebrate({
      [Segments.BODY]: authDataSchema,
    }),
    errors(),
  ]);
}
