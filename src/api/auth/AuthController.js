import RequestResponder from '../lib/validation/RequestResponder';
import User from '../../data/users/entities/User';
import generateToken from '../lib/passport/generateToken';

export default class AuthController {
  register = async (req, res, next) => {
    const {
      username,
      password,
    } = req.body;
    const {
      /** @type AuthService */
      authService,
    } = req.context;
    const requestResponder = new RequestResponder(res);

    try {
      const user = await authService.registerUser(new User({ username, password }));
      const token = generateToken(user.id);

      requestResponder.success({ user, token });
    } catch (err) {
      if (err.message === 'DUPLICATE_USERNAME') {
        requestResponder.failure('DUPLICATE_USERNAME');
      } else {
        next(err);
      }
    }
  };

  login = async (req, res, next) => {
    const {
      username,
      password,
    } = req.body;
    const {
      /** @type AuthService */
      authService,
    } = req.context;
    const requestResponder = new RequestResponder(res);

    try {
      const user = await authService.getUser(username, password);
      const token = generateToken(user.id);

      requestResponder.success({ user, token });
    } catch (err) {
      if (err.message === 'NOT_FOUND') {
        requestResponder.failure('NOT_FOUND');
      } else {
        next(err);
      }
    }
  }
}
