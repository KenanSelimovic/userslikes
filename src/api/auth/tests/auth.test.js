import { assert } from 'chai';

import {
  createUser,
  generateRandomUser,
  generateRandomUsername,
  getLoginToken,
} from '../../../tests/helpers/users';
import getSupertest from '../../../tests/helpers/supertest';
import iocContainer from '../../../iocContainer';

const supertest = getSupertest();
const {
  /** @type UsersDA */
  usersDA,
  /** @type AuthService */
  authService,
} = iocContainer.cradle;

describe('Auth controller', () => {
  describe('User registration', () => {
    it('registration_existingUsernameProvided_returns400', async () => {
      // arrange
      const username = 'test-user-for-test';
      await createUser({ username });
      const registrationData = { username, password: 'test-password' };

      // act
      const response = await supertest
        .post('/signup')
        .send(registrationData);

      // assert
      assert.equal(response.statusCode, 400);
    });
    it('registration_validData_returnsToken', async () => {
      // arrange
      const registrationData = { username: generateRandomUsername(), password: 'test-password' };

      // act
      const response = await supertest
        .post('/signup')
        .send(registrationData);

      // assert
      assert.equal(response.statusCode, 200);
      assert.exists(response.body.data.token);
    });
    it('registration_validData_doesntReturnPassword', async () => {
      // arrange
      const registrationData = { username: generateRandomUsername(), password: 'test-password' };

      // act
      const response = await supertest
        .post('/signup')
        .send(registrationData);

      // assert
      assert.equal(response.statusCode, 200);
      assert.notExists(response.body.data.user.password);
    });
    it('registration_validData_doesntStorePasswordInCleartext', async () => {
      // arrange
      const registrationData = { username: generateRandomUsername(), password: 'test-password' };

      // act
      await supertest
        .post('/signup')
        .send(registrationData);

      // assert
      const inserted = await usersDA.getByUsername(registrationData.username);
      assert.notEqual(inserted.password, registrationData.password);
    });
  });
  describe('User login', () => {
    it('login_validDataProvided_returnsToken', async () => {
      // arrange
      const password = 'random-password';
      const userData = generateRandomUser({ password });
      const user = await authService.registerUser(userData);

      // act
      const response = await supertest
        .post('/login')
        .send({ username: user.username, password });

      // assert
      assert.equal(response.statusCode, 200);
      assert.exists(response.body.data.token);
    });
    it('login_invalidDataProvided_doesntReturnToken', async () => {
      // arrange
      const password = 'random-password';
      const userData = generateRandomUser({ password });
      const user = await authService.registerUser(userData);

      // act
      const response = await supertest
        .post('/login')
        .send({ username: user.username, password: `${password}-wrong` });

      // assert
      assert.notExists(response.body.data);
    });
    it('login_validDataProvided_doesntReturnUserPassword', async () => {
      // arrange
      const password = 'random-password';
      const userData = generateRandomUser({ password });
      const user = await authService.registerUser(userData);

      // act
      const response = await supertest
        .post('/login')
        .send({ username: user.username, password });

      // assert
      assert.equal(response.statusCode, 200);
      assert.notExists(response.body.data.user.password);
    });
  });
  describe('Password change', () => {
    it('passwordChange_everythingOK_newPasswordUsableForLogin', async () => {
      // arrange
      const password = 'random-password';
      const userData = generateRandomUser({ password });
      const user = await authService.registerUser(userData);
      const newPassword = 'changed-password';
      const loginToken = getLoginToken(user);

      // act
      await supertest
        .post('/me/update-password')
        .set('Authorization', loginToken)
        .send({ password: newPassword })
        .expect(200);

      // assert
      await supertest
        .post('/login')
        .send({ username: userData.username, password: newPassword })
        .expect(200);
    }).timeout(50000);
  });
});
