import { Router } from 'express';

import Schemas from './AuthSchemasValidator';

export default (IOCContainer) => {
  const controller = IOCContainer.cradle.authController;
  return Router({ strict: true })
    .post('/signup', Schemas.register, controller.register)
    .post('/login', Schemas.login, controller.login);
};
