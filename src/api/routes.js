import express from 'express';

import logger from '../lib/logger';
import IOCContainer from '../iocContainer';

const router = express.Router({ strict: true });

router.use('*', (req, res, next) => {
  // create a scoped container
  // so that each request gets a new instance of dependency
  req.context = IOCContainer.createScope().cradle;
  next();
});

router.use('', require('../api/users/routes').default(IOCContainer));
router.use('', require('../api/auth/routes').default(IOCContainer));
router.use('', require('../api/me/routes').default(IOCContainer));

// error handler has to have 4 parameters for express to recognize it
// eslint-disable-next-line no-unused-vars
router.use((err, req, res, next) => {
  logger.error(
    err,
    {
      url: req.originalUrl,
      method: req.method,
      userAgent: req.header('User-Agent'),
    },
  );

  if (!res.headerSent) {
    res.status(500).send('Unknown error occurred').end();
  }
});

export default router;
