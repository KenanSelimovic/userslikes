export default class Env {
  /**
   * @param {string} key
   * @param {string} defaultValue
   * @param {boolean} isMandatory
   * @returns {string}
   */
  static getEnv = (key, defaultValue = '', isMandatory = false) => {
    const envValue = process.env[key] || defaultValue;
    if (isMandatory && envValue === '') {
      throw new Error(`Env variable ${key} is required`);
    }
    return envValue;
  };

  /**
   * @param {string} key
   * @param {number} defaultValue
   * @param {boolean} isMandatory
   * @returns {number}
   */
  static getEnvAsInt = (key, defaultValue, isMandatory = false) => parseInt(Env.getEnv(key, '', isMandatory), 10) || defaultValue;

  /**
   * @param {string} key
   * @param {boolean} defaultValue
   * @param {boolean} isMandatory
   * @returns {boolean}
   */
  static getEnvAsBool = (key, defaultValue = false, isMandatory = false) => (Env.getEnv(key, '', isMandatory) !== '' ? Env.getEnv(key) : defaultValue);
}
