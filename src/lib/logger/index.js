import winston from 'winston';

import config from '../../config';

const logger = winston.createLogger({
  level: config.logLevel,
  format: winston.format.simple(),
});


const baseTransportOptions = {
  colorize: true,
  silent: false,
  timestamp: true,
  handleExceptions: true,
};

if (config.isDevelopment() || config.isTesting()) {
  logger.add(new winston.transports.Console({
    ...baseTransportOptions,
    prettyPrint: true,
    level: config.logLevel,
    json: false,
  }));
} else {
  logger.add(new winston.transports.Console({
    ...baseTransportOptions,
    level: 'info',
    json: true,
    prettyPrint: false,
  }));
}

export default logger;
