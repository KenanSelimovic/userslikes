/* eslint global-require:0 */
import DB from './data/lib/db/DB';
import logger from './lib/logger';
import config from './config';

logger.info('Initializing DB and starting server');
// Create DB if it doesnt exist
DB.createDatabase(config.db.connection.database)
  .then(async () => {
    const bookshelfBootstrap = require('./data/lib/db/bookshelfBootstrap');
    const server = require('./api/server');

    if (config.databaseMigrationsAutorun) {
      await bookshelfBootstrap.migrate();
    }

    // Init Web Server
    server.serve();
  })
  .catch((err) => {
    logger.error(err);
    throw new Error('Could not create DB');
  });

// Catch unhandled exceptions.
process.on('uncaughtException', (err) => {
  logger.error('Uncaught exception event\n', err.stack);
});
