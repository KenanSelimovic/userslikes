require('@babel/register');
const config = require('./src/config').default;

module.exports = {
  development: config.db,
  staging: config.db,
  production: config.db,
};
